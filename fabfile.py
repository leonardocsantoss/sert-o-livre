from __future__ import with_statement
from fabric.api import *

@with_settings(warn_only=True)
@hosts("root@sertaolivre.org")
def deploy():
    with cd('/var/webapps/sertaolivre.org/sertaolivre/'):
        # get lastest version from git
        run('git pull')
        # restart nginx
        run('supervisorctl restart uepb')


@with_settings(warn_only=True)
@hosts("root@sertaolivre.org")
def deploy_migrate():
    with cd('/var/webapps/sertaolivre.org/sertaolivre/'):
        # get lastest version from git
        run('git pull')
        run('../bin/python manage.py syncdb --settings=sertaolivre.settings.production')
        run('../bin/python manage.py migrate --settings=sertaolivre.settings.production')
        # restart nginx
        run('supervisorctl restart uepb')