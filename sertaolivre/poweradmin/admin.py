# -*- coding: utf-8 -*-
import operator
from django.db import models
from django.contrib import admin, messages
from django.contrib.admin.util import flatten_fieldsets
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.core.urlresolvers import resolve
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django import template
from django.contrib.contenttypes.models import ContentType
from django.utils.safestring import mark_safe
from poweradmin import filters
from django import forms

import django.utils.simplejson as simplejson

from suit.widgets import *

from django.utils.text import get_text_list
from django.utils.translation import ugettext as _
from django.utils.encoding import force_unicode
from django.contrib.auth import get_permission_codename

'''
 Features:
 - novo filtro multi_search
 - list_filter no cabeçalho da página ao invés de ficar na lateral

 multi_search sintaxe:
     multi_search = (
        ('q1', 'Nome', ['disciplina__nome']),
        ('q2', 'E-mail', ['email']),
    )
'''


#Trim solution
class _BaseForm(object):
    def clean(self):
        for field in self.cleaned_data:
            if isinstance(self.cleaned_data[field], basestring):
               self.cleaned_data[field] = self.cleaned_data[field].strip()
        return super(_BaseForm, self).clean()

class BaseModelForm(_BaseForm, forms.ModelForm):
    pass


class PowerModelAdmin(admin.ModelAdmin):
    buttons = []
    multi_search = []
    list_select_related = True
    multi_search_query = {}
    queryset_filter = {}
    form = BaseModelForm
    formfield_overrides = {
        models.ForeignKey: {'widget': LinkedSelect(attrs={'class': 'input-medium'}) },
        models.IntegerField: {'widget': NumberInput(attrs={'class': 'input-small'}) },
        models.PositiveIntegerField: {'widget': NumberInput(attrs={'class': 'input-small'}) },
        models.DateField: {'widget': SuitDateWidget },
        models.TimeField: {'widget': SuitTimeWidget },
        models.DateTimeField: {'widget': SuitSplitDateTimeWidget },
        models.TextField: {'widget': AutosizedTextarea(attrs={'class': 'input-xxlarge'}),},
    }

    def has_change_permission(self, request, obj=None):
        opts = self.opts
        change = get_permission_codename('change', opts)
        view = get_permission_codename('view', opts)
        return request.user.has_perm("%s.%s" % (opts.app_label, change)) or request.user.has_perm("%s.%s" % (opts.app_label, view))

    def get_readonly_fields(self, request, obj=None):
        opts = self.opts
        change = get_permission_codename('change', opts)
        view = get_permission_codename('view', opts)
        if not request.user.has_perm("%s.%s" % (opts.app_label, change)) and request.user.has_perm("%s.%s" % (opts.app_label, view)):
            fields = []
            for field in list(flatten_fieldsets(self.get_fieldsets(request, obj))):
                if isinstance(field, str): fields.append(field)
                else: fields += field 
            return fields
        return self.readonly_fields

    def get_actions(self, request):
        actions = super(PowerModelAdmin, self).get_actions(request)
        #Ajustes no log do action delete_selected
        from poweradmin.actions import delete_selected
        actions['delete_selected'] = (delete_selected, 'delete_selected', delete_selected.short_description)
        return actions

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        #Verifica se a tela é readonly
        readonly = False
        readonly_fields = list(self.get_readonly_fields(request, obj))
        fields = []
        for field in list(flatten_fieldsets(self.get_fieldsets(request, obj))):
            if isinstance(field, str):
                fields.append(field)
            else:
                fields += field

        if set(fields) == set(readonly_fields).intersection(set(fields)):
            readonly = True
        
        for inline in context['inline_admin_formsets']:
            if set(flatten_fieldsets(inline.fieldsets)) != set(inline.readonly_fields).intersection(set(flatten_fieldsets(inline.fieldsets))):
                readonly = False

        opts = self.model._meta
        app_label = opts.app_label

        object_id = obj.pk if obj else obj
        buttons = self.get_buttons(request, object_id)

        context.update({
            'buttons': buttons,
            'readonly': readonly,
        })
        return super(PowerModelAdmin, self).render_change_form(request, context, add, change, form_url, obj)

    def construct_change_message(self, request, form, formsets):
        """
        Construct a change message from a changed object.
        """
        change_message = []
        if form.changed_data:
            changed_data_msg = []
            for field in form.changed_data:
                initial=form.initial.get(field)
                try: value=getattr(form.instance, field)
                except: value=form.initial.get(field)
                #ForeignKey
                try:
                    if type(form.instance._meta.get_field(field)) == models.fields.related.ForeignKey:
                        initial = getattr(form.instance, field).__class__.objects.get(pk=initial)
                except: pass
                #Choices
                try:
                    if type(form.instance._meta.get_field(field)) == models.fields.CharField and hasattr(form.instance._meta.get_field(field), 'choices'):
                        try: initial = dict(type(form.instance)._meta.get_field(field).get_choices())[initial]
                        except: pass
                        try: value = dict(type(form.instance)._meta.get_field(field).get_choices())[value]
                        except: pass
                except: pass
                if initial != value:
                    changed_data_msg.append(u'%s de %s para %s' % (force_unicode(field), force_unicode(initial), force_unicode(value)))
            if changed_data_msg:
                change_message.append(_(u'Changed %s.') % get_text_list(changed_data_msg, _('and')))

        if formsets:
            for formset in formsets:
                for added_object in formset.new_objects:
                    change_message.append(_(u'Added %(name)s "%(object)s".')
                                          % {'name': force_unicode(added_object._meta.verbose_name),
                                             'object': force_unicode(added_object)})
                for changed_object, changed_fields in formset.changed_objects:
                    for field in changed_fields:
                        try: value = getattr(changed_object, field)
                        except: value = '-'
                        change_message.append(_(u'Modificado %(field)s para %(value)s em "%(object)s".')
                                          % {'field': field,
                                             'value': value,
                                             'object': force_unicode(changed_object)})
                for deleted_object in formset.deleted_objects:
                    change_message.append(_(u'Deleted %(name)s "%(object)s".')
                                          % {'name': force_unicode(deleted_object._meta.verbose_name),
                                             'object': force_unicode(deleted_object)})
        change_message = ' '.join(change_message)
        return change_message or _(u'No fields changed.')

    def button_view_dispatcher(self, request, object_id, command):
        obj = self.model._default_manager.get(pk=object_id)
        return getattr(self, command)(request, obj)  \
            or HttpResponseRedirect(request.META['HTTP_REFERER'])

    def related_lookup(self, request):
        data = {}
        if request.method == 'GET':
            if request.GET.has_key('object_id'):
                try:
                    obj = self.queryset(request).get(pk=request.GET.get('object_id'))
                    data = {"value": obj.pk, "label": u"%s" % obj}
                except: pass
        return HttpResponse(simplejson.dumps(data), mimetype='application/javascript')

    def get_urls(self):
        opts = self.model._meta
        buttons_urls = [url(r'^(\d+)/(%s)/$' % but.flag, self.wrap(self.button_view_dispatcher)) for but in self.buttons]
        buttons_urls.append(url(r'^lookup/related/$', self.wrap(self.related_lookup), name="%s_%s_related_lookup" % (opts.app_label, opts.object_name.lower())))
        return patterns('', *buttons_urls) + super(PowerModelAdmin, self).get_urls()

    def wrap(self, view):
        from functools import update_wrapper
        def wrapper(*args, **kwargs):
            return self.admin_site.admin_view(view)(*args, **kwargs)
        return update_wrapper(wrapper, view)

    def get_buttons(self, request, object_id=None):
        return [b for b in self.buttons if b.visible]

    def get_changelist(self, request, **kwargs):
        from views import PowerChangeList
        return PowerChangeList

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context['buttons'] = self.get_buttons(request, None)

        opts = self.model._meta
        app_label = opts.app_label

        multi_search_fields = []
        for field_opts in self.multi_search:
            attributes = {
                'size': '40',
            }

            if len(field_opts) == 4:
                attributes.update(field_opts[3])

            multi_search_fields.append({
                'name': field_opts[0],
                'label': field_opts[1],
                'value': request.GET.get(field_opts[0], ''),
                'attributes': ' '.join(['%s="%s"' % (k, v) for k, v in attributes.items()]),
            })

        buttons = self.get_buttons(request, None)

        context_data = {
            'buttons': buttons,
            'multi_search': True,
            'multi_search_keys': multi_search_fields,
        }
        extra_context.update(context_data)
        return super(PowerModelAdmin, self).changelist_view(request, extra_context)


class PowerTabularInline(admin.TabularInline):
    formfield_overrides = {
        models.ForeignKey: {'widget': LinkedSelect(attrs={'class': 'input-medium'}) },
        models.IntegerField: {'widget': NumberInput(attrs={'class': 'input-small'}) },
        models.PositiveIntegerField: {'widget': NumberInput(attrs={'class': 'input-small'}) },
        models.DateField: {'widget': SuitDateWidget },
        models.TimeField: {'widget': SuitTimeWidget },
        models.DateTimeField: {'widget': SuitSplitDateTimeWidget },
        models.TextField: {'widget': AutosizedTextarea(attrs={'class': 'input-xxlarge'}),},
    }

    def has_change_permission(self, request, obj=None):
        opts = self.opts
        change = get_permission_codename('change', opts)
        view = get_permission_codename('view', opts)
        return request.user.has_perm("%s.%s" % (opts.app_label, change)) or request.user.has_perm("%s.%s" % (opts.app_label, view))

    def get_readonly_fields(self, request, obj=None):
        opts = self.opts
        change = get_permission_codename('change', opts)
        view = get_permission_codename('view', opts)
        if not request.user.has_perm("%s.%s" % (opts.app_label, change)) and request.user.has_perm("%s.%s" % (opts.app_label, view)):
            return opts.get_all_field_names()
        return self.readonly_fields


class PowerButton(object):
    flag = ''  # Usado para URLs fixas do botao, como na versao anterior
    url = ''  # Usado para informar diretamente a URL e assim permitir qualquer URL
    visible = True
    label = 'Label'
    attrs = {}

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def get_url(self):
        return self.url or (self.flag + '/')
