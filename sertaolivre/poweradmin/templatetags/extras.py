# -*- encoding:utf-8 -*-
from string import ascii_uppercase
from django import template
from django.db import models
from django.utils.safestring import mark_safe
from django.template.defaultfilters import slugify

register = template.Library()

@register.filter
def is_file(field):
    classe = field.model_admin.model._meta.get_field_by_name(field.field['name'])[0].__class__
    if classe in (models.FileField, models.ImageField):
        return True
    return False

@register.filter
def is_image(field):
    classe = field.model_admin.model._meta.get_field_by_name(field.field['name'])[0].__class__
    if classe == models.ImageField:
        return True
    return False

@register.filter
def required(label):
    if u'required' in label:
        label = label.replace(u':</label>', u'*:</label>')
    return mark_safe(label)