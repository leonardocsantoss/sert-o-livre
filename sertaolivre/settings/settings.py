# -*- coding: utf-8 -*-
"""
Django settings for sertaolivre project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os, sys
BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
sys.path.insert(0, BASE_DIR)

ADMINS = (
    ('Leonardo', 'leonardocsantoss@gmail.com'),
)

MANAGERS = ADMINS

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'h&v53*niepsrd4=mo60n@(dhy(32u5nn84o#lre#)z!uk014a#'

HOST = 'http://127.0.0.1:8000'

LOCAL = True
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django_localflavor_br',
    'poweradmin',
    'smart_selects',
    'south',
    'utils',
    'sertaolivre.site',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'utils.ThreadLocal.ThreadLocalMiddleware',
    'django.middleware.gzip.GZipMiddleware',
)

ROOT_URLCONF = 'sertaolivre.urls'

WSGI_APPLICATION = 'sertaolivre.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'sertaolivre.sqlite',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
ADMIN_MEDIA_ROOT = os.path.join(STATIC_ROOT, 'admin')

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'templates'),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

DEFAULT_FILE_STORAGE = 'utils.storage.SpecialCharFileSystemStorage'

#Configurations of the tools
ADMIN_TOOLS_MENU = 'sertaolivre.menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'sertaolivre.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'sertaolivre.dashboard.CustomAppIndexDashboard'

LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/'

#Suit
SUIT_CONFIG = {
    # header
    'ADMIN_NAME': u'Sertão Livre',
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',
    # menu
    'SEARCH_URL': '',
    'MENU_OPEN_FIRST_CHILD': True,
    'CONFIRM_UNSAVED_CHANGES': False,
    'MENU': (
        {'label': u'Matrículas', 'icon': 'icon-folder-open', 'models': (
            'core.matricula', 'core.periodo', 'core.turma',
        )},
        {'label': u'Configuração', 'icon': 'icon-cog', 'models': (
            'auth.user', 'auth.group', 'admin.logentry',
        )},
    ),
    'DASHBOARD': (
        {'label': u'Matrículas', 'icon': 'glyphicon glyphicon-folder-open', 'url': '/core/matricula/', 'permissions': ('core.change_matricula', 'core.view_matricula', )},
        {'label': u'Período', 'icon': 'glyphicon glyphicon-paperclip', 'url': '/core/periodo/', 'permissions': ('core.change_periodo', 'core.view_periodo',)},
        {'label': u'Turma', 'icon': 'glyphicon glyphicon-list-alt', 'url': '/core/turma/', 'permissions': ('core.change_turma', 'core.view_turma',)},
    ),
}