# -*- coding: utf-8 -*-
from django.contrib import admin, messages
from django.contrib.auth import get_permission_codename
from django.template.response import TemplateResponse
from django.conf.urls import patterns, include, url
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.core import serializers
from django.utils import simplejson
from django.core.urlresolvers import reverse
from django.core.exceptions import PermissionDenied
from django.contrib.admin.models import LogEntry
from django.template.loader import get_template
from django.template import RequestContext, loader, Context

from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin

from poweradmin.admin import PowerModelAdmin, PowerTabularInline, PowerButton

import cStringIO as StringIO
import cgi
from xhtml2pdf.pisa import pisaDocument

from models import *
from forms import *


# class TurmaInline(PowerTabularInline):
#     model = Turma
#     extra = 1
# class PeriodoAdmin(PowerModelAdmin):
#     list_display = ('periodo', '_actions', )
#     multi_search = (
#         ('q1', u'Período', ['periodo', ]),
#     )
#     inlines = (TurmaInline, )
#     fieldsets = [
#         (None, {'fields': ('periodo',), }),
#     ]
#     save_as = True
# admin.site.register(Periodo, PeriodoAdmin)


# class MatriculaTurmaTurmaInline(PowerTabularInline):
#     model = MatriculaTurma
#     fields = ('aluno_matricula', 'aluno', '_inline_actions', )
#     readonly_fields = ('aluno_matricula', 'aluno', '_inline_actions', )
#     extra = max_num = 0
#     can_delete = False
#     verbose_name_plural = u"Alunos matriculados"
# class TurmaHorarioInline(PowerTabularInline):
#     model = TurmaHorario
#     extra = 1
# class TurmaAdmin(PowerModelAdmin):
#     list_display = ('periodo', 'disciplina', 'sala', '_actions', )
#     list_filter = ('periodo', 'disciplina', 'sala', )
#     fieldsets = [
#         (None, {'fields': ('periodo', 'disciplina', 'sala', ), }),
#     ]
#     inlines = (TurmaHorarioInline, MatriculaTurmaTurmaInline, )
#     save_as = True

#     def imprimir(self, request, object_id, template_name='admin/core/turma/imprimir.html'):
#         template = get_template(template_name)
#         html  = template.render(RequestContext(request, {
#             'turma': self.get_object(request, object_id),
#             'STATIC_URL': settings.STATIC_URL,
#         }))

#         def fetch_resources(uri, rel):
#             path = os.path.join(settings.STATIC_ROOT, uri.replace(settings.STATIC_URL, ""))
#             return path

#         result = StringIO.StringIO()
#         pdf = pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources )
#         if not pdf.err:
#             return HttpResponse(result.getvalue(), mimetype='application/pdf')
#         return HttpResponse('We had some errors<pre>%s</pre>' % cgi.escape(html))

#     def get_urls(self):
#         urls_originais = super(TurmaAdmin, self).get_urls()
#         urls_customizadas = patterns('',
#             url(r'^imprimir/(?P<object_id>\d)/$', self.wrap(self.imprimir), name='core_turma_imprimir'),
#         )
#         return urls_customizadas + urls_originais

#     def get_buttons(self, request, object_id):
#         buttons = super(TurmaAdmin, self).get_buttons(request, object_id)
#         if object_id:
#             buttons.append(PowerButton(url=reverse('admin:core_turma_imprimir', kwargs={'object_id': object_id, }), label=u'Imprimir'))
#         return buttons
# admin.site.register(Turma, TurmaAdmin)


# class MatriculaTurmaInline(PowerTabularInline):
#     model = MatriculaTurma
#     formset = MatriculaTurmaInlineFormSet
#     extra = 1
# class MatriculaAdmin(PowerModelAdmin):
#     list_display = ('matricula', 'aluno', 'periodo', '_actions', )
#     list_filter = ('periodo',)
#     multi_search = (
#         ('q1', u'Matrícula', ['matricula', ]),
#         ('q2', u'Aluno', ['aluno', ]),
#     )
#     inlines = (MatriculaTurmaInline, )
#     fieldsets = [
#         (None, {'fields': ('matricula', 'aluno', 'periodo', ), }),
#     ]
#     save_as = True

#     def imprimir(self, request, object_id, template_name='admin/core/matricula/imprimir.html'):
#         template = get_template(template_name)
#         html  = template.render(RequestContext(request, {
#             'matricula': self.get_object(request, object_id),
#             'STATIC_URL': settings.STATIC_URL,
#         }))

#         def fetch_resources(uri, rel):
#             path = os.path.join(settings.STATIC_ROOT, uri.replace(settings.STATIC_URL, ""))
#             return path

#         result = StringIO.StringIO()
#         pdf = pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources )
#         if not pdf.err:
#             return HttpResponse(result.getvalue(), mimetype='application/pdf')
#         return HttpResponse('We had some errors<pre>%s</pre>' % cgi.escape(html))

#     def get_urls(self):
#         urls_originais = super(MatriculaAdmin, self).get_urls()
#         urls_customizadas = patterns('',
#             url(r'^imprimir/(?P<object_id>\d)/$', self.wrap(self.imprimir), name='core_matricula_imprimir'),
#         )
#         return urls_customizadas + urls_originais

#     def get_buttons(self, request, object_id):
#         buttons = super(MatriculaAdmin, self).get_buttons(request, object_id)
#         if object_id:
#             buttons.append(PowerButton(url=reverse('admin:core_matricula_imprimir', kwargs={'object_id': object_id, }), label=u'Imprimir'))
#         return buttons

# admin.site.register(Matricula, MatriculaAdmin)


class LogEntryAdmin(PowerModelAdmin):
    search_fields = ('object_repr', 'change_message', 'user__username', )
    list_filter = ('action_time', 'content_type', 'action_flag',)
    list_display = ('action_time', 'user', 'content_type', 'tipo', 'object_repr',)
    fields = ('action_time', 'user', 'content_type', 'object_id', 'object_repr', 'action_flag', 'tipo', 'change_message', )
    readonly_fields = ('action_time', 'user', 'content_type', 'object_id', 'object_repr', 'action_flag', 'tipo', 'change_message', )
    multi_search = (
        ('q1', u'Repr. do Objeto', ['object_repr', ]),
        ('q2', u'Mensagem', ['change_message', ]),
        ('q3', u'User', ['user__username', ]),
    )
    def tipo(self, obj):
        if obj.is_addition():
            return u"1-Adicionado"
        elif obj.is_change():
            return u"2-Modificado"
        elif obj.is_deletion():
            return u"3-Deletado"
admin.site.register(LogEntry, LogEntryAdmin)



admin.site.unregister(Group)
class CustomGroupAdmin(GroupAdmin):
    list_display = ('name', '_actions', )
    save_as = True
    def _actions(self, modeladmin):
        acoes = u'<div class="col-actions" style="width: 77px;">'
        acoes += u'<a href="%s" class="icon-edit" alt="%s"></a>' % (modeladmin.pk, u"Editar")
        acoes += u'<a href="javascript://" action="delete_selected" class="action-link icon-trash" object-id="%s" alt="%s"></a>' % (modeladmin.pk, u"Remover")
        acoes += u'</div>'
        return acoes
    _actions.allow_tags = True
    _actions.short_description = u"Ações"

admin.site.register(Group, CustomGroupAdmin)



admin.site.unregister(User)
class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', '_actions', )
    save_as = True
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (u'Informações pessoais', {'fields': ('first_name', 'last_name', 'email')}),
        (u'Permissões', {'fields': ('groups', )}),
    )
    superuser_fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (u'Informações pessoais', {'fields': ('first_name', 'last_name', 'email')}),
        (u'Permissões', {'fields': ('is_active', 'is_superuser', 'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2')}
        ),
    )

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        if request.user.is_superuser:
            return self.superuser_fieldsets
        return super(CustomUserAdmin, self).get_fieldsets(request, obj)

    def _actions(self, modeladmin):
        acoes = u'<div class="col-actions" style="width: 77px;">'
        acoes += u'<a href="%s" class="icon-edit" alt="%s"></a>' % (modeladmin.pk, u"Editar")
        acoes += u'<a href="javascript://" action="delete_selected" class="action-link icon-trash" object-id="%s" alt="%s"></a>' % (modeladmin.pk, u"Remover")
        acoes += u'</div>'
        return acoes
    _actions.allow_tags = True
    _actions.short_description = u"Ações"

    def save_model(self, request, obj, form, change):
        obj.is_staff = True
        super(CustomUserAdmin, self).save_model(request, obj, form, change)
        if not request.user.is_superuser and Instituicao.objects.filter(usuarios=request.user).exists():
            for inst in Instituicao.objects.filter(usuarios=request.user):
                inst.usuarios.add(obj)

    def queryset(self, request):
        qs = super(CustomUserAdmin, self).queryset(request)
        if request.user.is_superuser:
            return qs
        if Instituicao.objects.filter(usuarios=request.user).exists():
            usuarios_ids = []
            for inst in Instituicao.objects.filter(usuarios=request.user):
                usuarios_ids += list(inst.usuarios.values_list('pk', flat=True))
            qs = qs.filter(pk__in=usuarios_ids)
        return qs
admin.site.register(User, CustomUserAdmin)