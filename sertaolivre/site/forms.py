# # -*- coding: utf-8 -*-
# from django import forms
# from django.db.models import Q
# from django.db import IntegrityError
# from smart_selects.form_fields import ChainedModelChoiceField
# from django.forms.models import BaseInlineFormSet

# from models import *

# class MatriculaTurmaInlineFormSet(BaseInlineFormSet):
#     def clean(self):
#         super(MatriculaTurmaInlineFormSet, self).clean()
#         #Verifica o choque de horário
#         for form in self.forms:
#             if not form.is_valid(): return
#             if form.cleaned_data and not form.cleaned_data.get('DELETE'):
#                 for form2 in self.forms:
#                     if not form2.is_valid(): return
#                     if form2.cleaned_data and not form2.cleaned_data.get('DELETE') and form2 != form:
#                         horarios = form.cleaned_data['turma'].turmahorario_set.all()
#                         for horario2 in form2.cleaned_data['turma'].turmahorario_set.all():
#                             if horarios.filter(dia=horario2.dia, hora=horario2.hora):
#                                 raise forms.ValidationError(u'Choque de horário entre as turmas %s e %s.' % (form.cleaned_data['turma'], form2.cleaned_data['turma']))