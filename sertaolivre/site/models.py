# # -*- coding: utf-8 -*-
# from django.db import models
# from django.contrib.auth.models import User, Group
# from django.db.models import signals
# from django.dispatch import receiver
# from django.conf import settings
# from django.core.exceptions import ValidationError
# from django.core.urlresolvers import reverse

# from smart_selects.db_fields import ChainedForeignKey, GroupedForeignKey

# from datetime import datetime
# import os


# class Periodo(models.Model):
#     class Meta:
#         ordering = ('-periodo', )
#         verbose_name = u'Período'
#         verbose_name_plural = u'Períodos'
#         permissions = (("view_periodo", u"Can view Período"),)

#     periodo = models.CharField(u'Período', max_length=6, unique=True)

#     def _actions(self):
#         acoes = u'<div class="col-actions" style="width: 77px;">'
#         acoes += u'<a href="%s" class="icon-edit" data-toggle="tooltip" data-placement="bottom" title="%s"></a>' % (self.pk, u"Editar")
#         acoes += u'<a href="javascript://" action="delete_selected" class="action-link icon-trash" object-id="%s" data-toggle="tooltip" data-placement="bottom" title="%s"></a>' % (self.pk, u"Remover")
#         acoes += u'</div>'
#         return acoes
#     _actions.allow_tags = True
#     _actions.short_description = u"Ações"

#     def __unicode__(self):
#         return u'%s' % self.periodo


# class Turma(models.Model):
#     class Meta:
#         ordering = ('-disciplina', )
#         verbose_name = u'Turma'
#         verbose_name_plural = u'Turmas'
#         permissions = (("view_turma", u"Can view Turma"),)
#         unique_together = (('periodo', 'disciplina',))

#     periodo = models.ForeignKey(Periodo, verbose_name=u'Período')
#     disciplina = models.CharField(u'Disciplina', max_length=256)
#     sala = models.CharField(u'Sala', max_length=12, blank=True, null=True)

#     def _actions(self):
#         acoes = u'<div class="col-actions" style="width: 77px;">'
#         acoes += u'<a href="%s" class="icon-edit" data-toggle="tooltip" data-placement="bottom" title="%s"></a>' % (self.pk, u"Editar")
#         acoes += u'<a href="javascript://" action="delete_selected" class="action-link icon-trash" object-id="%s" data-toggle="tooltip" data-placement="bottom" title="%s"></a>' % (self.pk, u"Remover")
#         acoes += u'</div>'
#         return acoes
#     _actions.allow_tags = True
#     _actions.short_description = u"Ações"

#     def __unicode__(self):
#         return u'%s' % (self.disciplina, )

# DIA = (
#     ('0', u'Domingos'),
#     ('1', u'Segundas'),
#     ('2', u'Terças'),
#     ('3', u'Quartas'),
#     ('4', u'Quintas'),
#     ('5', u'Sextas'),
#     ('6', u'Sábados'),
# )
# class TurmaHorario(models.Model):
#     class Meta:
#         ordering = ('dia', 'hora', )
#         verbose_name = u'Horário'
#         verbose_name_plural = u'Horários'
#         permissions = (("view_turmahorario", u"Can view Horário"),)

#     turma = models.ForeignKey(Turma, db_index=True)
#     dia = models.CharField(u'Dia', max_length=1, choices=DIA)
#     hora = models.TimeField(u'Hora')

#     def __unicode__(self):
#         return u'%s às %s' % (self.get_dia_display(), self.hora)


# class Matricula(models.Model):
#     class Meta:
#         ordering = ('-periodo', 'matricula', )
#         verbose_name = u'Matrícula'
#         verbose_name_plural = u'Matriculas'
#         permissions = (("view_matricula", u"Can view Matrícula"),)
#         unique_together = (('matricula', 'periodo', ))

#     aluno = models.CharField(u'Aluno', max_length=256)
#     matricula = models.CharField(u'Matrícula', max_length=120)
#     periodo = models.ForeignKey(Periodo, verbose_name=u'Período')

#     def _actions(self):
#         acoes = u'<div class="col-actions" style="width: 77px;">'
#         acoes += u'<a href="%s" class="icon-edit" data-toggle="tooltip" data-placement="bottom" title="%s"></a>' % (self.pk, u"Editar")
#         acoes += u'<a href="javascript://" action="delete_selected" class="action-link icon-trash" object-id="%s" data-toggle="tooltip" data-placement="bottom" title="%s"></a>' % (self.pk, u"Remover")
#         acoes += u'</div>'
#         return acoes
#     _actions.allow_tags = True
#     _actions.short_description = u"Ações"

#     def __unicode__(self):
#         return u'%s/%s' % (self.matricula, self.aluno)


# class MatriculaTurma(models.Model):
#     class Meta:
#         verbose_name = u'Matrícula/Turma'
#         verbose_name_plural = u'Matrícula/Turmas'
#         permissions = (
#             ("view_matriculaturma", u"Can view Matrícula/Turma"),
#         )
#         unique_together = (('matricula', 'turma', ), )

#     matricula = models.ForeignKey(Matricula, db_index=True)
#     turma = ChainedForeignKey(Turma, db_index=True, chained_fields={'periodo': 'periodo',}, show_all=False, auto_choose=False, verbose_name=u'Turma')
    
#     def aluno(self):
#         return self.matricula.aluno

#     def aluno_matricula(self):
#         return self.matricula.matricula
#     aluno_matricula.short_description = u'Matrícula'

#     def _inline_actions(self):
#         acoes = u'<div class="col-actions" style="width: 77px;">'
#         acoes += u'<a href="%s" style="border: 0;padding: 0;background-color: transparent;" class="icon-edit" data-toggle="tooltip" data-placement="bottom" title="%s"></a>' % (reverse('admin:core_matricula_change', args=(self.matricula.pk, )), u"Editar", )
#         acoes += u'</div>'
#         return acoes
#     _inline_actions.allow_tags = True
#     _inline_actions.short_description = u"Ações"

#     def __unicode__(self):
#         return u'%s/%s' % (self.matricula, self.turma)