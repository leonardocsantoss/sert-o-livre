# -*- coding: utf-8 -*-
from django import template
from django.utils import simplejson
from easy_thumbnails.files import get_thumbnailer
import feedparser


register = template.Library()


@register.simple_tag
def chart_data(request, user):
    data = [[u'Período', u'Alunos',],]
    # for periodo in Periodo.objects.all():
    #     data.append([u'%s' % periodo, periodo.matricula_set.count()])
    return simplejson.dumps(data)


@register.assignment_tag(takes_context=True)
def feed_data(context, feed='http://www.uepb.edu.br/feed/'):
    return feedparser.parse(feed)


@register.filter
def has_perms(perms, permissions):
    for perm in permissions:
        if perm in perms:
            return True
    return False