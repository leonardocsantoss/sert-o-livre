# coding: utf-8
import mimetypes
from django.views.generic import TemplateView


class LandpageView(TemplateView):
    template_name = 'site/landpage.html'
